import burger from './modules/burger';
import xhibition from './modules/xhibition';
import modal from './modules/modal';
import selectFunc from './modules/select';

window.addEventListener('DOMContentLoaded', () => {
    'use strict';

    burger('.burger', '.nav', 'is-active', 'is-active');
    (location.pathname == '/xhibition-page.html') ? xhibition() : null;
    modal('.button-sertificat', '.modal_sertificat', 'is-active', '.modal_sertificat-close');
    selectFunc();
});

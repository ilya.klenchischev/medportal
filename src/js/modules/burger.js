const burger = (trigger, nav, activeClassBurger, activeClassNav) => {
  const burgerButton = document.querySelector(trigger),
  menu = document.querySelector(nav);

  if (!burgerButton && !menu) return;

  burgerButton.addEventListener('click', (e) => {
    e.preventDefault();
    burgerButton.classList.toggle(activeClassBurger);
    menu.classList.toggle(activeClassNav);
  });
}

export default burger;
const modal = (trigger, box, activeClass, closeTrigger) => {
  const buttons = document.querySelectorAll(trigger),
        popUp = document.querySelector(box),
        close = document.querySelector(closeTrigger);

  if (!buttons) return;
  console.log(buttons);
  buttons.forEach(button => {
    button.addEventListener('click', (e) => {
      e.preventDefault();
      popUp.classList.add(activeClass);
    });
  });

  close.addEventListener('click', () => {
    popUp.classList.remove(activeClass);
  });
};


export default modal;
const selectFunc = () => {
  const selects = document.querySelectorAll('.select');

  selects.forEach(select => {
    let head = select.querySelector('.select__head'),
    body = select.querySelector('.select__body'),
    simplebar = body.querySelector('.simplebar-content');
    head.addEventListener('click', (e) => {
      head.classList.toggle('is-active');
      body.classList.toggle('is-active');
    });
    simplebar.children.forEach(item => {
      item.addEventListener('click',() => {
        let option = item.textContent;
        head.querySelector('.select__option-active').textContent = option;
        body.classList.toggle('is-active');
        head.classList.toggle('is-active');
      })
    })
  });
};

export default selectFunc;